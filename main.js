//Shazer Rizzo Varela
//This Program is to make a randomized roller for dnd stats. Adaping code from Toolset1

//DND Rules
const defaultAttributeScores = [15, 14, 13, 12, 10, 8];

class Player {

    //Creates the class for the character and variables for stats
    constructor(characterName = 'Naruto'){
        this.name = characterName;
        this.attributes = {
            strength: 0,
            dexterity: 0,
            constitution: 0,
            intelligence: 0,
            wisdom: 0,
            charisma: 0
        };

        //Variations of Scores
        let shuffledResult = shuffleArray(defaultAttributeScores);
        for (const [key, value] of Object.entries(this.attributes)) {
            let attributeValue = shuffledResult.pop();
            this.attributes[key] = attributeValue;
        }
    }

    // Starts to roll the randomized attributes
    rollAttributes(){
        for (const key in this.attributes) {
            let results = diceRoller(4, 6);
            results.sort(function(a, b){return a - b}); // numeric sort w/ compare function
            results.shift(); // remove lowest die roll
            let sum = sumArrayElements(results); // sum the rolls
            this.attributes[key] = sum;
        }
    }

    // Prints the Character and it Attributes
    printPlayer(){
        console.log(`NAME: ${this.name}`);
        for (const [key, value] of Object.entries(this.attributes)) {
            console.log(`${key.slice(0, 3).toUpperCase()}: ${value}`);
        }
    }

}

//Creates the Player 1
const player01 = new Player();
player01.printPlayer();
//Creates PLayer 2
const player02 = new Player('Son Goku');
player02.rollAttributes();
player02.printPlayer();

// Fisher-Yates algorithm for randomly sorting an array
// from: https://dev.to/codebubb/how-to-shuffle-an-array-in-javascript-2ikj
// adapted to JS and reconfigured to return a new (non-mutated) array
function shuffleArray(targetArray) {
    let shuffled = Array.from(targetArray);
    for (let i = shuffled.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const temp = shuffled[i];
        shuffled[i] = shuffled[j];
        shuffled[j] = temp;
    }
    return shuffled;
}

//Customizing the dice and amount of times
function diceRoller(times, sides) {
    let results = [];
    for (let i = 0; i < times; i++) {
        results.push(Math.floor(Math.random() * sides + 1));
    }

    return results;
}

//Drop the lowest of the four. Body can be replaced with: return array.reduce((total, currentNumber) => total + currentNumber);
function sumArrayElements(array) {
    let sum = 0;
    for (let i = 0; i < array.length; i++) {
        sum += array[i];
    }
    
    return sum;
}
